import requests
import re



# header = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'}

header = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0'}
# proxies = {'http': '113.76.96.252',  'https': '222.222.169.60'}


def download(url):
    try:
        response = requests.get(url, headers=header, )
        if response.status_code == 200:
            page = re.search('ref=(?P<page>.+)\?', url).groupdict()
            filename = page['page']+'.html'
            filename_1 = filename.replace('/','-')
            f = open('download\\'+filename_1, 'w', encoding='utf-8')
            f.write(response.content.decode('utf-8'))
            f.close()
            return filename_1
        else:
            return "error"
    except Exception as e:
        print(e)
        return "error"

# download('https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dtools&field-keywords=led%2Bheadlight')
# download('https://www.amazon.com/s/ref=sr_pg_2?fst=p90x%3A1&rh=n%3A228013%2Ck%3Aled%2Bheadlight&page=2&keywords=led%2Bheadlight&ie=UTF8&qid=1525922141')
# download('https://www.amazon.com/s/ref=sr_pg_3?fst=p90x%3A1&rh=n%3A228013%2Ck%3Aled%2Bheadlight&page=3&keywords=led%2Bheadlight&ie=UTF8&qid=1525933638')