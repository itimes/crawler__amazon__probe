import time
import random
from down import download
from page_parsing import page_parsing

result_down = download('https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dtools&field-keywords=led%2Bheadlight')

while result_down != 'error':
        print(result_down)
        result_parse = page_parsing("download\\"+result_down)
        print(result_parse)
        if len(result_parse) > 0:
            sleep_time = random.randint(4, 6)
            time.sleep(sleep_time)
            url = "https://www.amazon.com"+result_parse[0]
            print(url)
            result_down = download(url)

        else:
            break