import os
import re
import time
import json
from lxml import etree


def page_parsing(site):
    if os.path.isfile(site):
        f = open(site, 'r', encoding='utf-8')
        content = f.read()
        f.close()
        html = etree.HTML(content.encode('utf-8'))
        link_one = html.xpath('//*[@id="s-results-list-atf"]/li[1]/@id')[0]
        link = link_one.split('_')

        name=link[0]
        start_num = int(link[1])
        query_num = start_num
        stop_num = int(link[1]) + 40

        #用来保存数据
        data = []

        while query_num < stop_num:
            # asin号
            query_asin = html.xpath('//*[@id="'+name+'_'+str(query_num)+'"]/@data-asin')
            if len(query_asin) > 0:
                # ref qid keywords
                query_content = html.xpath('//*[@id="'+name+'_'+str(query_num)+'"]//a/@href')
                if len(query_content) > 0:
                    c1 = query_content[0]
                    if re.search('.+ref.+qid.+keyword.+', c1):
                        if c1.startswith('https://'):
                            result = re.search("ref=(?P<ref>.+)/.+qid=(?P<qid>\d+)&.+keywords=(?P<keywords>.+)",c1).groupdict()
                            query_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(result['qid'])))

                            result['qid'] = query_time
                            result['asin'] = query_asin[0]
                            #是否是广告，1代表是，0代表不是
                            result['is_adv'] = 0
                            print(result)
                            data.append(result)

                        elif c1.startswith('/gp'):
                            result = re.search('ref=(?P<ref>.+)\?.+26qid%3D(?P<qid>\d+)%26.+26keywords%3D(?P<keywords>.+)%26', c1).groupdict()
                            query_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(result['qid'])))

                            result['qid'] = query_time
                            result['asin'] = query_asin[0]
                            # 是否是广告，1代表是，0代表不是
                            result['is_adv'] = 1
                            print(result)
                            data.append(result)

            query_num += 1

        # 把data写入到文件中
        print(len(data))
        filename = site.split(os.sep)[-1].split('.')[0]+'.txt'
        with open('data\\'+filename, 'w',encoding='utf-8') as f1:
            json.dump(data, f1)


        #返回下一页的位置：
        next_page = html.xpath('//*[@id="pagnNextLink"]/@href')
        return next_page

# page_parsing('download\\nb_sb_noss.html')
# page_parsing('download\\sr_pg_3.html')